<?php
namespace getinstance\commons\model;

use PHPUnit\Framework\TestCase;

class ModelRecordTest extends \PHPUnit_Framework_TestCase 
{

    public function testUnknownField() {
        $stub = $this->getMockForAbstractClass('\\getinstance\\commons\\model\\ModelRecord');       
        try {
            $stub->getNonexistent();
            self::fail("should have thrown exception");
        } catch (\Exception $e) {
            self::assertTrue($e instanceof \getinstance\commons\exceptions\UnknownFieldException);
        }
    }

    public function testUnconstrained() {
        $child = $this->getBlurper([
            "pants" => "green",
            "topempty1"=>null,
            "topempty2"=>0,
            "topempty3"=>"",
            "address" => [
                "street" => "sycamore",
            ],
            "emptytest" => [
                "fullish" => 1,
                "nullish" => null,
                "empty" => ""
            ]
        ]);

        // get variations
        self::assertEquals($child->getField("pants"), "green");
        self::assertEquals($child->pants(), "green");
        self::assertEquals($child->pants, "green");
        self::assertEquals($child->getPants(), "green");

        self::assertEquals($child->getField("address/street"), "sycamore");
        self::assertEquals(call_user_func([$child, "address/street"]), "sycamore");
        self::assertEquals($child->getField("address"), [ "street" => "sycamore" ]);

        // truthy tests
        self::assertEquals($child->truthy("emptytest/nullish"), false);
        self::assertEquals($child->truthy("emptytest/empty"), false);
        self::assertEquals($child->truthy("emptytest/notthere"), false);
        self::assertEquals($child->truthy("emptytest/fullish"), true);
        self::assertEquals($child->truthy("topempty1"), false);
        self::assertEquals($child->truthy("topempty2"), false);
        self::assertEquals($child->truthy("topempty3"), false);
        self::assertEquals($child->truthy("pants"), true);

        // default if no match
        self::assertEquals($child->getField("walrus", "awful"), "awful");

        // default if no match
        self::assertEquals($child->getField("address/notthere", "awful"), "awful");

        // set field
        self::assertEquals($child->setField("address/town", "gondor", false), "gondor");
        $out = $child->toFullArray();
        self::assertEquals($out['address']['town'], "gondor");
    }

    public function testRequired() {
        $rows = [
            "pants" => "green",
            "address" => [
                "street" => "sycamore"
            ]
        ];
        $child = $this->getBlurper($rows, ["pants"]);
    }

    function getBlurper($rows, $required=[]) {
        $child = new class($rows, $required) extends ModelRecord {
            private $required;
            function __construct($rows, $required) {
                $this->required = $required;
                parent::__construct($rows);
            }

            public function requiredFields() {
                return $this->required;
            }

        };
        return $child;
    }

}
