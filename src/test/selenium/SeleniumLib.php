<?php
namespace getinstance\commons\test\selenium;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class SeleniumLib {
    function __construct( \RemoteWebDriver $driver ) {
        $this->driver = $driver;
    }

    function partialHrefExists( $partial ) {
        $xpath = "//a[contains(@href, '{$partial}')]";
        try {
            $link= $this->driver->findElement( \WebDriverBy::xpath( $partial ) );
            return true;
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    function waitForPartialHref( $partial ) {
        $xpath = "//a[contains(@href, '{$partial}')]";
        return $this->waitForXpathVisible( $xpath );
    }

    function clickLinkAtPartialHref( $partial ) {
        $this->waitForPartialHref($partial);

        $xpath = "//a[contains(@href, '{$partial}')]";
        $sub = $this->driver->findElement( \WebDriverBy::xpath($xpath) );
        $sub->sendkeys("\n");
        return $sub;
    }

    function waitForLink($partial) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::partialLinkText( $partial )
            )
        );
    }

    function clickLinkExact( $partial ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::linkText( $partial )
            )
        );
        $link= $this->driver->findElement( \WebDriverBy::LinkText( $partial ) );
        $this->myclick($link);
        return $link;
    }

    function clickLink( $partial ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::partialLinkText( $partial )
            )
        );
        $link= $this->driver->findElement( \WebDriverBy::partialLinkText( $partial ) );
        $this->myclick($link);
        return $link;
    }
    function idVisible($id) {
        if (! $this->idExists($id)) {
            return false;
        }
        $el= $this->driver->findElement( \WebDriverBy::id( $id) );
        return $el->isDisplayed();
    }

    function nameExists($name) {
        try {
            $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
            return true;
        } catch ( \Exception $e) {
            return false;
        }
    }

    function idExists($id) {
        try {
            $el= $this->driver->findElement( \WebDriverBy::id( $id ) );
            return true;
        } catch ( \Exception $e) {
            return false;
        }
    }

    function classExists($id) {
        try {
            $el = $this->driver->findElement( \WebDriverBy::className( $id ) );
            return true;
        } catch ( \Exception $e) {
            return false;
        }
    }

    function linkVisible($partial) {
        if (! $this->linkExists($partial)) {
            return false;
        }
        $link = $this->driver->findElement( \WebDriverBy::partialLinkText( $partial ) );
        return $link->isDisplayed();
    }

    function linkExists($partial) {
        try {
            $this->waitForLink($partial);
            $link= $this->driver->findElement( \WebDriverBy::partialLinkText( $partial ) );
            return true;
        } catch ( \Exception $e) {
            return false;
        }
    }

    function attributeForId($id, $attribute) {
        $this->waitForId($id);
        $el = $this->driver->findElement( \WebDriverBy::id( $id  ) );
        $val = $el->getAttribute($attribute);
        return $val;
    }

    function clickAtName( $name ) {
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        $this->myclick($el);
        return $el;
    }

    function clickAtId( $id) {
        $this->waitForId( $id );
        $el = $this->driver->findElement( \WebDriverBy::id( $id ) );
        $this->myclick($el);
        return $el;
    }

    function clickAtCss( $selector ) {
        $this->waitForCss( $selector );
        $schemel = $this->driver->findElement( \WebDriverBy::cssSelector( $selector ) );
        $this->myclick($schemel);
        return $schemel;
    }

    function clickAtClass( $classname) {
        $this->waitForClass( $classname );
        $schemel = $this->driver->findElement( \WebDriverBy::className( $classname ) );
        $this->myclick($schemel);
        return $schemel;
    }

    function clickAtAttributeWithVal( $key, $val ) {
        $xpath = "//*[@{$key}='{$val}']";
        $this->waitForXpath( $xpath );
        $sub = $this->driver->findElement(
            \WebDriverBy::xpath( $xpath ) );
        $this->myclick($sub);
        return $sub;
    }

    // not currently used - treat as untested
    function clickParent( $name ) {
        $sub = $this->driver->findElement( \WebDriverBy::xpath(
            "//*[@name='{$name}']/.."
        ) );
        $this->myclick($sub);
    }

    function clickAtXpath( $xpath ) {
        $this->waitForXpath( $xpath );
        $sub = $this->driver->findElement( \WebDriverBy::xpath( $xpath) );
        $this->myclick($sub);
        return $sub;
    }

    function clickElContainingText( $txt ) {
        $sub = $this->driver->findElement( \WebDriverBy::xpath(
            "//*[contains(text(),'{$txt}')]"
        ) );
        $this->myclick($sub);
        return $sub;
    }

    function clickButton( $val ) {
        $myexception;

        $css_vals = array(
            "input[type='submit'][value='{$val}']",
            "button[value='{$val}']"
        );
        foreach ( $css_vals as $css ) {
            try {
                $sub = $this->driver->findElement( \WebDriverBy::cssSelector($css) );
                if ( is_object( $sub ) ) {
                    $this->myclick($sub);
                    return;
                }
            } catch ( \Exception $e ) {
                $myexception = $e;
            }
        }
        throw $myexception;
    }

    function myclick($el) {
        $el->sendKeys("\n");
    }

    function switchToWin( $title ) {
        $found = false;
        $handles = $this->driver->getWindowHandles();
        foreach ( $handles as $handle ) {
            $this->driver->switchTo()->window( $handle );
            if ( $this->driver->getTitle() == $title ) {
                $found = true;
                continue;
            }
        }
        return $found;
    }

    function typeInAtClass( $id, $val ) {
        $this->waitForClass( $id );
        $schemel = $this->driver->findElement( \WebDriverBy::classname( $id ) );
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    function typeInAtId( $id, $val ) {
        $this->waitForId( $id );
        $schemel = $this->driver->findElement( \WebDriverBy::id( $id ) );
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    function typeIn( $name, $val ) {
        $this->waitForName( $name );
        $schemel = $this->driver->findElement( \WebDriverBy::name( $name ) );
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    function clickAtClassFirstVisible( $classname ) {
        $els = $this->driver->findElements( \WebDriverBy::className( $classname ) );
        $callback = function( $subel ) {
            $this->myclick($subel);
        };
        return $this->doFirstVisible( $classname, $els, $callback );
    }

    function clickCheckFirstVisible( $name ) {
        $els = $this->driver->findElements( \WebDriverBy::name( $name ) );
        $callback = function( $subel ) {
            $this->myclick($subel);
        };
        return $this->doFirstVisible( $name, $els, $callback );
    }

    function typeInFirstVisible( $name, $val ) {
        $els = $this->driver->findElements( \WebDriverBy::name( $name ) );
        $callback = function( $subel ) use ( $val ) {
            $subel->clear();
            $subel->sendKeys($val);
        };
        return $this->doFirstVisible( $name, $els, $callback );
    }

    function doFirstVisible( $name, array $els, callable $callback ) {
        foreach( $els as $subel ) {
            if ( $subel->isDisplayed() ) {
                $callback( $subel );
                return $subel;
            }
        }
        throw new \Exception( "no visible element identified by: $name" );
    }

    function typeInAtXpath( $xpath, $val ) {
        $schemel = $this->driver->findElement( \WebDriverBy::xpath( $xpath ) );
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    function pickFromSelect( $name, $val ) {
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        return $this->pickFromSelectAtElement( $el, $val );
    }

    function pickFromSelectAtXpath( $xpath, $val ) {
        $el = $this->driver->findElement( \WebDriverBy::xpath( $xpath ) );
        return $this->pickFromSelectAtElement( $el, $val );
    }

    function pickFromSelectAtId( $id, $val ) {
        $el = $this->driver->findElement( \WebDriverBy::xpath( "//select[@id='{$id}']" ) );
        return $this->pickFromSelectAtElement( $el, $val );
    }

    function getElementByName( $name ) {
        $this->waitForName( $name );
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        return $el;
    }

    function getElementByClass( $name ) {
        $this->waitForClass( $name );
        $el = $this->driver->findElement( \WebDriverBy::classname( $name ) );
        return $el;
    }

    function getElementsByClass( $name ) {
        $this->waitForClass( $name );
        $el = $this->driver->findElements( \WebDriverBy::classname( $name ) );
        return $el;
    }

    function getElementValueByName( $name ) {
        $el = $this->getElementByName($name);
        return $el->getText();
    }

    function getElementValueAttributeByName( $name ) {
        $this->waitForName($name);
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        return $el->getAttribute("value");
    }

    function getInnerElementValueByClass($class, $tag){
        $xpath = "//div[contains(@class, '".$class."')]/".$tag;
        $el = $this->driver->findElement( \WebDriverBy::xpath( $xpath ) );
        return $el;
    }

    function getInnerElementValueById($id, $tag){
        $xpath = "//div[contains(@id, '".$id."')]/".$tag;
        $el = $this->driver->findElement( \WebDriverBy::xpath( $xpath ) );
        return $el;
    }

    function findId( $id ) {
        $this->waitForId($id);
        $el = $this->driver->findElement( \WebDriverBy::id( $id ) );
        return $el;
    }

    function getElementValueAttribute( $id ) {
        $this->waitForId($id);
        $el = $this->driver->findElement( \WebDriverBy::id( $id ) );
        return $el->getAttribute("value");
    }

    function getElementValueByClass( $id ) {
        $this->waitForClass($id);
        $el = $this->driver->findElement( \WebDriverBy::className( $id ) );
        return $el->getText();
    }

    function getElementValue( $id ) {
        $this->waitForId($id);
        $el = $this->driver->findElement( \WebDriverBy::id( $id ) );
        return $el->getText();
    }

    function getSelectedValueAttribute( $name ) {
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        $select = new \WebDriverSelect( $el );
        $option = $select->getFirstSelectedOption();
        return $option->getAttribute("value");
    }

    function getSelectedValueById( $name ) {
        $this->waitForId($name);
        $el = $this->driver->findElement( \WebDriverBy::id( $name ) );
        $select = new \WebDriverSelect( $el );
        $option = $select->getFirstSelectedOption();
        return $option->getText();
    }

    function getSelectedValue( $name ) {
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        $select = new \WebDriverSelect( $el );
        $option = $select->getFirstSelectedOption();
        return $option->getText();
    }

    function pickIndexFromSelect( $name, $idx ) {
        $el = $this->driver->findElement( \WebDriverBy::name( $name ) );
        $select = new \WebDriverSelect( $el );
        $select->selectByIndex( $idx );
    }

    function pickFromSelectAtElement( $el, $val ) {
        $select = new \WebDriverSelect( $el );
        $select->selectByValue( $val );
        return $el;
    }

    function clickHiddenCheckById( $id ) {
        print "trying to get id - '$id'\n";
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").checked = true;", array());
    }

    function clickCheck( $name ) {
        $check = $this->driver->findElement( \WebDriverBy::name($name) );
        $this->myclick($check);
        return $check;
    }

    function clickCheckAtXpath( $xpath ) {
        $check = $this->driver->findElement( \WebDriverBy::xpath($xpath) );
        $this->myclick($check);
    }

    function clickRadio( $name, $value ) {
        $check = $this->driver->findElement( \WebDriverBy::xpath("//input[@name='{$name}'][@value='{$value}']") );
        $this->myclick($check);
    }

    function clickHouse( $name ) {
        $check = $this->driver->findElement( \WebDriverBy::xpath("//input[@name='{$name}']/following-sibling::span") );
        $this->myclick($check);
    }

    function clickHouseAtId( $name ) {
        $check = $this->driver->findElement( \WebDriverBy::xpath("//input[@id='{$name}']/following-sibling::span") );
        $this->myclick($check);
    }

    function waitForName( $name ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::name($name) )
        );
    }

    function waitForNameVisible( $name ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::name($name) )
        );
    }

    function waitForClassVisible( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::classname($id) )
        );
    }

    function waitForIdVisible( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::id($id) )
        );
    }

    function waitForCssVisible( $css ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::cssSelector($css) )
        );
    }

    function waitForXpathVisible( $xpath ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::xpath($xpath) )
        );
    }

    function waitForId( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::id($id) )
        );
    }

    function waitForClass( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::presenceOfElementLocated(
                \WebDriverBy::className($id) )
        );
    }

    function waitForClassVisibility( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::className($id) )
        );
    }

    function waitForIdVisibility( $id ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::id($id) )
        );
    }

    function waitForXpath( $xpath ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::xpath($xpath) )
        );
    }

    function waitForCss( $xpath ) {
        $this->driver->wait(20, 1000)->until(
            \WebDriverExpectedCondition::visibilityOfElementLocated(
                \WebDriverBy::cssSelector($xpath) )
        );
    }

    function waitForSelectToFillThenPickIndex( $name, $idx ) {
        $el = $this->driver->findElement(\WebDriverBy::name( $name ) );

        // this is a dynamically populated pulldown
        // don't select an element until there are some
        // elements to choose from
        $select = new \WebDriverSelect( $el );
        $this->driver->wait(20, 1000)->until(
            function( $driver ) use ( $select ) {
                $opts = $select->getOptions();
                if ( count( $opts ) > 2 ) { return true; }
                return false;
            }, "failed to get element count"
        );
        $this->pickIndexFromSelect( $name, $idx );
    }

    function getEdit($name) {
        sleep(2);
        $css = "textarea[name='{$name}'] + div div[class='note-editable']";
        $this->waitForCssVisible( $css );
        $el = $this->driver->findElement( \WebDriverBy::cssSelector( $css ) );
        return $el;
    }

    function getEditText($name) {
        $el = $this->getEdit($name);
        return $el->getText();
    }

    function typeEdit( $name, $val ) {
        $el = $this->getEdit($name);
        $el->clear();
        $el->sendKeys($val);
    }

    function clickRadarCheck( $id ) {
        $selector = "input[id='{$id}']";
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").click();", array());
    }

    function radarCheckIsSelected($id) {
        $xpath = "//input[@id='{$id}']";
        $el = $this->driver->findElement( \WebDriverBy::xpath( $xpath ) );
        $checked = $el->getAttribute("CHECKED");
        return ($checked == "true")?true:false;
    }

    function jsClickRadio($id) {
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").click();", array());
    }

    function jsClickRadioByName($name) {
        $element = $this->driver->findElements(\WebDriverBy::name($name));
        if($element) {
            $element[0]->click();
        }
    }

    function jsSetValueAtId($id, $val) {
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").value=\"{$val}\";", array());
    }

}

?>
