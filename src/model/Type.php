<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\model;

use getinstance\commons\exceptions\UnknownFieldException;

abstract class Type {

    function check($a) {
        if (! $this->doCheck($a)) {
            $class = get_class($this);
            $type = gettype($a);
            throw new \Exception("type mismatch. incoming type: $type / checker: $class");
        }
    }

    abstract function doCheck($a);
    abstract function convertToScalar($a);
    abstract function convertFromScalar($a);
}
