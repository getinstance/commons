<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\model;

use getinstance\commons\exceptions\UnknownFieldException;

abstract class ModelRecord {
    protected $fields;
    private $sep = "/";

    public function __construct( array $row=[], $validate=true ) {
        $this->fields= $row; 
        if ($validate) {
            $this->validateConstructorArray();
        }

        // entityfields should exist
        foreach ($this->entityFields() as $entitykey) {
            if (! isset($this->fields[$entitykey])) {
                $this->fields[$entitykey] = null;
            }
        }
    }
   
    public function deleteField($key) {
        $required = $this->requiredFields();
        if ( array_key_exists($key, $required) ) {
            throw new \Exception( "{$field}' is required for ".get_class( $this ) );
        }
        unset($this->fields[$key]);
    }

    public function validateConstructorArray() {
        foreach ( $this->requiredFields() as $field ) {
            if ( ! array_key_exists( $field, $this->fields ) ) {
                throw new UnknownFieldException( "required field '{$field}' missing for ".get_class( $this ) );
            }
        }

        foreach ( static::enforcedTypes() as $field => $typeobj ) {
            if ( ! array_key_exists( $field, $this->fields ) ) {
                continue;
            }
            $typeobj->check($this->fields[$field]);
        }

        foreach($this->fields as $field => $val) {
            if (method_exists($this, "set{$field}")) {
                return call_user_func([$this, "set{$field}"], $val);
            }
        }
    }

    public function isNull() {
        return false;
    }

    public function truthy( $key ) {
        try {
            list($subkey, $fields) =  $this->resolveKey($key);
        } catch (\Exception $e) {
            return false;
        }

        if ( ! $this->fieldExists( $key ) ) {
            return false;
        }
        $val = $this->getField($key);
        if ( empty( $val ) ) {
            return false;
        }
        return true;
    }

    public function fieldExists( $key ) {
        try {
            list($key, $fields) =  $this->resolveKey($key);
        } catch (\Exception $e) {
            return false;
        }
        if ( array_key_exists( $key, $fields ) ) {
            return true;
        }
        return false;
    }

    public function fieldIs( $key, $val ) {
        if ( ! $this->fieldExists( $key ) ) {
            return false;
        }
        return ($this->getField($key) == $val);
    }

    private function checkType($key, $val) {
        $types = static::enforcedTypes();
        if (isset($types[$key])) {
            $typecheck =  $types[$key];
            $typecheck->check($val);
        }
    }

    protected function forceSetField($key, $val) {
        $this->checkType($key, $val);
        return $this->fields[$key] = $val;
    }

    public function setField($key, $val, $strict = true) {
        $this->checkType($key, $val);
        $fieldkeys = $this->entityFields();
        if ((! in_array($key, $fieldkeys)) && $strict) {
            throw new UnknownFieldException("attempt to set unknown field: '$key'");
        }
        // if the child class is enforcing type with a setter - call it
        if (method_exists($this, "set{$key}")) {
            return call_user_func([$this, "set{$key}"], $val);
        } else {
            return $this->generateKeyVal($key, $val);
        }
    }

    protected function generateKeyVal($key, $val) {
        $subkeys = explode($this->sep, $key);
        $pointer = &$this->fields;
        foreach ($subkeys as $x) {
            if (! array_key_exists($x, $pointer)) {
                $pointer[$x] = [];
            }
            $lastpointer = &$pointer;
            $lastkey = $x;
            $pointer = &$pointer[$x];
        }
        $lastpointer[$lastkey] = $val;
        return $val;
    }

    protected function resolveKey($key) {
        $subkeys = explode($this->sep, $key);
        $pointer = $this->fields;
        foreach ($subkeys as $x) {
            if (! array_key_exists($x, $pointer)) {
                throw new \Exception("unable to resolve key {$key}");
            }
            $lastpointer = &$pointer;
            $lastkey = $x;
            $pointer = &$pointer[$x];
        }
        return [$lastkey, $lastpointer];
    }

    public function getField($key, $default=null ) {
        if ( ! is_null($default) && ! $this->truthy($key) ) {
            return $default;
        }
        list($subkey, $fields) =  $this->resolveKey($key);
        if ( ! array_key_exists( $subkey, $fields ) ) {
            return null;
        }
        return $fields[$subkey];
    }

    public function __get( $var ) {
        if (! $this->fieldExists($var)) {
            throw new UnknownFieldException("Unknown field '$var' for ".get_class( $this ) ); 
        }
        return $this->getField($var);
    }

    public function __call( $method, $args ) {
        $method = strtolower($method);
        if ($this->fieldExists($method)) {
            return $this->getField($method);
        }

        if ( preg_match("/^get(.*)/i", $method, $matches) &&
             $this->fieldExists($matches[1])  
        ) {
            return $this->getField($matches[1]);
        }

        if ( preg_match("/^set(.*)/i", $method, $matches)) 
        {
            $key = $matches[1];
            if (method_exists($this, "set{$key}")) {
                return call_user_func([$this, "set{$key}"], $val);
            }
            $this->setField($matches[1], $args[0]);
            return;
        }

        throw new UnknownFieldException("Unknown field '$method' for ".get_class( $this ) ); 
    }

    public function requiredFields() {
        return array();
    }

    public static function entityFields() {
        return array();
    }

    public static function enforcedTypes() {
        return array();
    }

    public function getFields() {
        $ret = []; 
        foreach( $this->entityFields() as $key) {
            if (isset($this->fields[$key])) {
                $ret[$key] = $this->fields[$key];
            }
            //$ret[$key] = isset($this->fields[$key])?$this->fields[$key]:null;
        }
        return $ret;
    }

    public function toScalarArray() {
        $fields = $this->getFields();
        $ret = [];
        $types = static::enforcedTypes();
        foreach ($fields as $key => $val) {
            if (isset($types[$key])) {
                $ret[$key] = $types[$key]->convertToScalar($val);
            } else {
                $ret[$key] = $val;
            }
        }
        return $ret;
    }

    public static function fromScalarArray(array $row) {
        // $fields = $this->getFields();
        $ret = [];
        $types = static::enforcedTypes();
        foreach ($row as $key => $val) {
            if (isset($types[$key])) {
                $ret[$key] = $types[$key]->convertFromScalar($val);
            } else {
                $ret[$key] = $val;
            }
        }
        return new static($ret);
    }

    public function toFullArray() {
        return $this->fields;
    }

    public function toArray() {
        return $this->getFields();
    }

}
