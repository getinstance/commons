<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\model;

use getinstance\commons\exceptions\UnknownFieldException;

class DateType extends Type {

    function docheck($a) {
        if ($a instanceof \DateTime) {
            return true;
        }
        return false;
    }

    function convertToScalar($a) {
        $this->check($a);
        return $a->format("Y-m-d H:i:s");
    }

    function convertFromScalar($a) {
        $converted = new \DateTime($a);
        $this->check($converted);
        return $converted;
    }
}
