<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\model;

use getinstance\commons\exceptions\UnknownFieldException;

class ScalarType extends Type {
    private $type;

    function __construct(string $type) {
        $this->type = $type;
    }

    function docheck($a) {
        if (gettype($a) == $this->type) {
            return true;
        }
        return false;
    }

    function convertToScalar($a) {
        settype($a, $this->type);
        return $a;
    }

    function convertFromScalar($a) {
        settype($a, $this->type);
        return $a;
    }
}
