<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\model;

use getinstance\commons\exceptions\UnknownFieldException;

class SerialisableArrayType extends Type {

    function docheck($a) {
        if (is_array($a)) {
            return true;
        }
        return false;
    }

    function convertToScalar($a) {
        return json_encode($a);
    }

    function convertFromScalar($a) {
        return json_decode($a);
    }
}
