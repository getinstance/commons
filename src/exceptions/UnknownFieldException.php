<?php
/*
 * This file is part of the getinstance/commons library.
 *
 * (c)2018 getInstance ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\commons\exceptions;

class UnknownFieldException extends \Exception {
    public function __construct($msg, $code=null) {
        parent::__construct($msg, $code); 
    }
}
